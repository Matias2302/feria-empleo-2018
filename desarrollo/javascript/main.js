// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });


$(document).ready(function(){
  $(window).scroll(function(){
    var scrollTop = $(window).scrollTop();
    if (scrollTop > 249) {
        $('.header').removeClass('fadeInLeft');
        $('.navbar').addClass('position');
        $('.navbar').addClass('fadeInDown');
    }
    else {
        $('.navbar').removeClass('position');
        $('.header').addClass('fadeInLeft');
    }
  });
});
$(document).ready(function(){
    $('.charlas-slider').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 3
            }
        }]
    });
        // slidesToShow: 6,
        // slidesToScroll: 1,
        // autoplay: true,
        // autoplaySpeed: 1500,
        // arrows: false,
        // dots: false,
        // pauseOnHover: false,
        // responsive: [{
        //     breakpoint: 768,
        //     settings: {
        //         slidesToShow: 4
        //     }
        // }, {
        //     breakpoint: 520,
        //     settings: {
        //         slidesToShow: 3
        //     }
        // }]
    });
